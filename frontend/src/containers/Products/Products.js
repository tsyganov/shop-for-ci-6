import React, {Component} from 'react';
import {fetchProducts} from "../../store/actions/productsActions";
import {connect} from "react-redux";
import {Link, NavLink as RouterNavLink} from "react-router-dom";
import ProductListItem from "../../components/ProductListItem/ProductListItem";
import ShowTo from "../../hoc/ShowTo";
import {fetchCategories} from "../../store/actions/categoriesActions";
import DrawerLayout from "../../components/UI/DrawerLayout/DrawerLayout";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

class Products extends Component {
  componentDidMount() {
    this.props.fetchProducts(this.props.match.params.id);
    this.props.fetchCategories();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.props.fetchProducts(this.props.match.params.id);
    }
  }

  render() {
    const drawerContent = (
      <MenuList>
        <MenuItem
          component={RouterNavLink}
          to="/" exact
          activeClassName="Mui-selected"
        >
          All products
        </MenuItem>

        {this.props.categories.map(category => (
          <MenuItem
            key={category._id}
            component={RouterNavLink}
            to={"/category/" + category._id}
            activeClassName="Mui-selected"
          >
            {category.title} ({category.productCount})
          </MenuItem>
        ))}
      </MenuList>
    );

    return (
      <DrawerLayout drawerContent={drawerContent}>
        <Grid container direction="column" spacing={2}>

          <Grid item container direction="row" justify="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h4">
                Products
              </Typography>
            </Grid>
            <Grid item>
              <ShowTo role="admin">
                <Button
                  color="primary"
                  component={Link}
                  to={"/products/new"}
                >
                  Add product
                </Button>
              </ShowTo>
            </Grid>
          </Grid>

          <Grid item container direction="row" spacing={1}>
            {this.props.products.map(product => (
              <ProductListItem
                key={product._id}
                title={product.title}
                id={product._id}
                price={product.price}
                image={product.image}
              />
            ))}
          </Grid>
        </Grid>

      </DrawerLayout>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products.products,
  categories: state.categories.categories,
});

const mapDispatchToProps = dispatch => ({
  fetchProducts: categoryId => dispatch(fetchProducts(categoryId)),
  fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
