import React, {Component, Fragment} from 'react';
import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct, fetchTags} from "../../store/actions/productsActions";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

class NewProduct extends Component {
  componentDidMount() {
    this.props.fetchCategories();
    this.props.fetchTags();
  }

  createProduct = async (productData) => {
    await this.props.createProduct(productData);
    this.props.history.push('/');
  };

  render() {
    return (
      <Fragment>
        <Box pb={2} pt={2}>
          <Typography variant="h4">New product</Typography>
        </Box>

        <ProductForm
          onSubmit={this.createProduct}
          categories={this.props.categories}
          tags={this.props.tags}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categories.categories,
  tags: state.products.tags
});

const mapDispatchToProps = dispatch => ({
  createProduct: productData => dispatch(createProduct(productData)),
  fetchCategories: () => dispatch(fetchCategories()),
  fetchTags: () => dispatch(fetchTags()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);