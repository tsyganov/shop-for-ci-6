import axiosApi from "../../axiosApi";
import {
  LOGIN_USER_REQUEST,
  loginUserFailure, loginUserSuccess,
  REGISTER_USER_REQUEST,
  registerUserFailure,
  registerUserSuccess
} from "../actions/usersActions";
import {put, takeEvery} from "redux-saga/effects";
import {push} from "connected-react-router";
import {toast} from "react-toastify";

function* registerUser({userData}) {
  try {
    yield axiosApi.post('/users', userData);
    yield put(registerUserSuccess());
    yield put(push('/'));
    toast.success('Registered successfully');
  } catch (error) {
    yield put(registerUserFailure(error.response.data));
  }
}

function* loginUser({userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginUserSuccess(response.data));
    toast.success('Logged in successfully');
    yield put(push('/'));
  } catch (error) {
    yield put(loginUserFailure(error.response.data));
  }
}

export default [
  takeEvery(REGISTER_USER_REQUEST, registerUser),
  takeEvery(LOGIN_USER_REQUEST, loginUser)
];

