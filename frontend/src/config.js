const env = process.env.REACT_APP_ENV;

let apiURL = 'http://localhost:8000';
const facebookAppId = "535807590649987";

if (env === 'test') {
  apiURL = 'http://localhost:8010';
}

if (env === 'production') {
  apiURL = '/api';
}

const config = {
  apiURL,
  facebookAppId,
};

export default config;