const fs = require('fs');
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const products = require('./app/products');
const categories = require('./app/categories');
const users = require('./app/users');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const run = async () => {
  await mongoose.connect(config.database, config.databaseOptions);

  app.use('/products', products);
  app.use('/categories', categories);
  app.use('/users', users);

  if (isNaN(config.bind)) {
    console.log('Trying to bind to a unix socket');

    fs.stat(config.bind, err => {
      console.log('In stat, ', err);
      if (!err) {
        console.log('no errors');
        fs.unlinkSync(config.bind);
      }

      app.listen(config.bind, () => {
        fs.chmodSync(config.bind, '777');
        console.log(`HTTP Server started on ${config.bind}`);
      })
    });
  } else {
    app.listen(config.bind, () => {
      console.log(`HTTP Server started on ${config.bind}`);
    });
  }
};

run().catch(e => {
  console.error(e);
});