const path = require('path');

const rootPath = __dirname;

const env = process.env.NODE_ENV;

let database = 'mongodb://localhost/shop';
let bind = 8000;

if (env === 'test') {
  database = 'mongodb://localhost/shop-test';
  bind = 8010;
}

if (env === 'production') {
  database = 'mongodb://localhost/shop-prod';
  bind = '/sockets/api.sock'
}

module.exports = {
  bind,
  rootPath,
  uploadPath: path.join(rootPath, 'public'),
  database,
  databaseOptions: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  },
  facebook: {
    appId: '535807590649987',
    appSecret: 'fcbf695b0c27a9c35626c6afc89ff347'
  }
};